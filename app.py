from flask import Flask, render_template
from flask_wtf import CSRFProtect
from flask_pymongo import PyMongo
from auth import auth
from auth.auth import login_required

app = Flask(__name__)
app.config['SECRET_KEY'] = "my_secret_key"
app.config["MONGO_DBNAME"] = "fuelingdb"
app.config["MONGO_URI"] = "mongodb://localhost:27017/fuelingdb"
csrf = CSRFProtect()
app.register_blueprint(auth.bp)
mongo = PyMongo(app)


@app.route('/')
@login_required
def index():
    return render_template("base.html")


if __name__ == '__main__':
    csrf.init_app(app)
    app.run(debug=True)
