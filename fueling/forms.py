from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, FloatField, validators, SelectField, TextAreaField


class TipoCombustibleForm(FlaskForm):
    tip_nombre = SelectField("Tipo de combustible", [validators.required(message="El tipo del combustible es requerido")])
    tip_precio = FloatField("Precio del combustible por litro", [validators.required(message="El precio es requerido")])


class CargaCombustibleForm(FlaskForm):
    horometro = StringField("Horometro")
    odometro = StringField("Odometro")
    surt_nombre = StringField("Nombre de la estación de servicios")
    surt_ubicacion = StringField("Ubicacion de la estación de servicios")
    surt_desc = TextAreaField("Descripcion(opcional)")
