from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, validators


class LoginForm(FlaskForm):
    username = StringField("Nombre de usuario",
                           [
                               validators.required(message="EL nombre de usuario es requerido"),
                               validators.length(min=4, max=25, message="El nombre de usuario es invalido!")
                           ])
    password = PasswordField("Contraseña",
                             [
                                 validators.required(message="La contraseña es requerida"),
                                 validators.length(min=8, max=60, message="La contraseña es invalida!")
                             ])


class RegisterForm(FlaskForm):
    name = StringField("Nombres", [validators.required(message="Se necesitan los nombres")])
    last_name = StringField("Apellidos", [validators.required(message="Se necesitan los apellidos")])
    username = StringField("Nombre de usuario",
                           [
                               validators.required(message="EL nombre de usuario es requerido"),
                               validators.length(min=4, max=25, message="El nombre de usuario es invalido!")
                           ])
    password = PasswordField("Contraseña",
                             [
                                 validators.required(message="La contraseña es requerida"),
                                 validators.length(min=8, max=60, message="La contraseña es invalida!")
                             ])
