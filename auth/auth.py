import functools
from flask import Blueprint, session, g, render_template, url_for, request, redirect, flash
from auth.forms import LoginForm, RegisterForm
from werkzeug.security import check_password_hash, generate_password_hash


bp = Blueprint('auth', __name__, url_prefix="/auth")


def get_id_user():
    from app import mongo
    id = mongo.db.users.count()
    return id


@bp.route("/login", methods=['GET', 'POST'])
def login():
    from app import mongo
    users = mongo.db.users
    login_form = LoginForm(request.form)
    error = None
    if request.method == 'POST':
        existing_user = users.find_one({"username": login_form.username.data}, {"_id": 0})
        print(existing_user)
        if existing_user is None:
            error = "El usuario es incorrecto"
        elif not check_password_hash(existing_user['password'], login_form.password.data):
            error = "La contraseña es incorrecta"

        if error is None:
            session.clear()
            session['user_id'] = existing_user['id']
            return redirect(url_for('index'))

        flash(error)

    return render_template("auth/login.html", form=login_form)


@bp.route("/register", methods=['GET', 'POST'])
def register():
    from app import mongo
    users = mongo.db.users
    register_form = RegisterForm(request.form)
    error = None
    if request.method == 'POST':
        user = users.find_one({"username": register_form.username.data}, {"_id": 0})
        print(user)
        if user is not None:
            error = "Usuario {} ya se encuetra registrado".format(register_form.username.data)

        elif error is None:
            new_user = {
                "id": get_id_user() + 1,
                "name": register_form.name.data,
                "last_name": register_form.last_name.data,
                "username": register_form.username.data,
                "password": generate_password_hash(register_form.password.data)
            }
            mongo.db.users.insert_one(new_user)
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template("auth/register.html", form=register_form)


@bp.route("/logout")
def logout():
    session.clear()
    return redirect(url_for('auth.login'))


@bp.before_app_request
def load_logged_in_user():
    from app import mongo
    user_log = mongo.db.users
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = user_log.find_one({"id": user_id}, {"_id": 0})


def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view
